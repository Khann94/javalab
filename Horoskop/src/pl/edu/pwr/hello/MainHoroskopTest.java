package pl.edu.pwr.hello;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.hamcrest.*;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

import java.util.Calendar;
import java.util.Random;

public class MainHoroskopTest {
 
	Horoskop horoskopTest = new Horoskop();
	
	@Before
	public void initialize(){
		horoskopTest.setImie("Emil");
		horoskopTest.setNazwisko("Klepacz");
	}
	
	@Test
	public void shouldBeGoodProphecy() {
		assertThat("Wprowadź do swego życia więcej radości." , is(equalTo(horoskopTest.wrozbaZdrowie())));
	}
	
	@Test
	public void shoudBeGoodDayofMonth() {
		Calendar calendar = Calendar.getInstance();
		assertThat(calendar.get(Calendar.DAY_OF_MONTH), Matchers.lessThan(32));
	}
	
	@Test
	public void shoudRandomNumberBeLessThan12() {
		Random r = new Random();
		assertThat(r.nextInt(12), Matchers.lessThan(12));
	}
	
	@Test
	public void CzyTablicePrzepowiedniMajaPoDwanasciePrzepowiedni(){
		Assert.assertTrue(horoskopTest.getPrzepowiednie().MILOSC.length == 12);
		Assert.assertTrue(horoskopTest.getPrzepowiednie().PRACA.length == 12);
		Assert.assertTrue(horoskopTest.getPrzepowiednie().ZDROWIE.length == 12);
	}
	
	@Test
	public void CzyindexPracaJestMiedzyZeroAJedenascie(){
		Assert.assertTrue(0 <= horoskopTest.zwrocindexPraca() && horoskopTest.zwrocindexPraca() <= 11);
	}
	
	}
