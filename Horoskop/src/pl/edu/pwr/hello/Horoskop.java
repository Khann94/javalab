package pl.edu.pwr.hello;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.Random;

public class Horoskop {
	String imie = null;
	String nazwisko = null;
	Przepowiednie przepowiednie = new Przepowiednie();

	public Przepowiednie getPrzepowiednie() {
		return przepowiednie;
	}
	
	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String wrozbaZdrowie() {
		int index = (this.imie.length() + this.nazwisko.length()) % 12;
		String wrozba = Przepowiednie.ZDROWIE[index];
		return wrozba;
	}

	public String wrozbaMilosc() {
		Calendar calendar = Calendar.getInstance();
		int dzien = calendar.get(Calendar.DAY_OF_MONTH);
		int index = (dzien - 1) % 12;
		String wrozba = Przepowiednie.MILOSC[index];
		return wrozba;
	}

	public String wrozbaPraca() {
		Random r = new Random();
		int index = r.nextInt(12);
		String wrozba = Przepowiednie.PRACA[index];
		return wrozba;
	}
	
	public int zwrocindexPraca(){
		Random r = new Random();
		int index = r.nextInt(12);
		return index;
	}

	public void wczytajImieINazwisko() {
		try (BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in))) {

			System.out.println("Podaj swoje imie:");
			String imie = bufferRead.readLine();
			setImie(imie);

			System.out.println("Podaj swoje nazwisko:");
			String nazwisko = bufferRead.readLine();
			setNazwisko(nazwisko);

			System.out.println("Witaj " + imie + " " + nazwisko);

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
